package com.camera;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableNativeArray;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TakePictureModule extends ReactContextBaseJavaModule {
    private Socket socket = null;
    private static final String MGS = "LGI:SMIO01";
     public static final String SERVER_IP = "103.127.207.216";
//    public static final String SERVER_IP = "192.168.100.111";
     public static final int SERVER_PORT = 10118;
//    public static final int SERVER_PORT = 8080;
    public static final int timeout = 10000;

    public String resultSocket = "";

    //constructor
    public TakePictureModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    //Mandatory function getName that specifies the module name
    @Override
    public String getName() {
        return "TakePictureArray";
    }

    @ReactMethod
    public void getResultLogin(Callback successCallback) {
        connectSocket();
        String prefix =  "LGI:" + getSerialNumber();
        connectLogin(prefix, successCallback);
    }

    @ReactMethod
    public void getResultCheckin(String base64, Callback successCallback) {
        connectSocket();
        String prefix =  "CKI:" + getDeviceID() + ":";
        sendImage(prefix, base64, successCallback);
    }

    @ReactMethod
    public void getResultCheckout(String base64, Callback successCallback) {
        connectSocket();
        String prefix =  "CKO:" + getDeviceID() + ":";
        sendImage(prefix, base64, successCallback);
    }

    //Custom function that we are going to export to JS
    @ReactMethod
    public void getBytesArray(String urlImage, Callback successCallback, Callback errorCallback) {
        Uri uri = Uri.parse(urlImage);
        WritableArray a = new WritableNativeArray();
        Context context = getReactApplicationContext();
        InputStream iStream;
        byte[] inputData = null;
        try {
            iStream = context.getContentResolver().openInputStream(uri);
            inputData = getBytes(iStream);
            if (inputData != null) {
                for (int item : inputData) {
                    a.pushInt(item);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            errorCallback.invoke(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            errorCallback.invoke(e.getMessage());
        }
        successCallback.invoke(a);
    }

    private byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public void uriToByteArray(String uri) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(uri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        byte[] buf = new byte[1024];
        int n;
        try {
            while (-1 != (n = fis.read(buf)))
                baos.write(buf, 0, n);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bytes = baos.toByteArray();
    }

    private void connectSocket() {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                SocketAddress socketAddress = new InetSocketAddress(SERVER_IP, SERVER_PORT);
                socket = new Socket();
                try {
                    // Timeout required - it's in milliseconds
                    socket.connect(socketAddress, timeout);

                } catch (SocketTimeoutException ex) {
                    Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
                }
                return socket.isConnected();
            }

            @Override
            protected void onPostExecute(Boolean isConnectSocket) {
                super.onPostExecute(isConnectSocket);
                Log.e("connect", isConnectSocket + "");
//                connectLogin();
            }
        }.execute();
    }

    private void connectLogin(String prefix, Callback successCallback) {
        new AsyncTask<Void, Void, String>() {
            String out = "";

            @Override
            protected String doInBackground(Void... voids) {
                try {
                    DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
                    Log.e("line prefix", prefix);
                    Log.e("line MGS", MGS);
                    outToServer.writeUTF(prefix.toString());
                    outToServer.flush();

                    BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String line = "";

                    while ((line = is.readLine()) != null) {
                        Log.e("line", line);
                        if (line.equals("</IOSec>")) {
                            out += line;
                            return out;
                        } else {
                            out += line + "\n";
                        }
                    }
                    outToServer.close();
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return out;
            }

            @Override
            protected void onPostExecute(String textResult) {
                super.onPostExecute(textResult);
                Log.e("line login", textResult);
                successCallback.invoke(textResult);
//                setResultText(textResult);

            }
        }.execute();
    }

    public byte[] getByteImage(String urlImage) {
        Uri uri = Uri.parse(urlImage);
        WritableArray a = new WritableNativeArray();
        Context context = getReactApplicationContext();
        InputStream iStream;
        byte[] inputData = new byte[0];
        try {
            iStream = context.getContentResolver().openInputStream(uri);
            inputData = getBytes(iStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return inputData;
    }

    public void sendImage(String prefix, String urlImage, Callback successCallback) {
        new AsyncTask<Void, Void, String>() {
            String out = "";
            @Override
            protected String doInBackground(Void... voids) {
                String out = "";
                Log.e("prefix", prefix);
                try {
                    DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
                    outToServer.writeUTF(prefix + getByteImage(urlImage).length+ ":");
                    Thread.sleep(50);
                    outToServer.write(getByteImage(urlImage));

                    outToServer.flush();

                    BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String line;
                    while ((line = is.readLine()) != null) {
                        Log.e("line", line);
                        if (line.equals("</IOSec>")) {
                            out += line;
                            return out;
                        } else {
                            out += line + "\n";
                        }
                    }

                    outToServer.close();
                    is.close();
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return out;
            }

            @Override
            protected void onPostExecute(String textResult) {
                super.onPostExecute(textResult);
                successCallback.invoke(textResult);
            }
        }.execute();
    }

    public String getDeviceID() {
        Context context = getReactApplicationContext();
        String m_androidId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return m_androidId;
    }

    public String getSerialNumber() {
        String serialNumber;

        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);

            serialNumber = (String) get.invoke(c, "gsm.sn1");
            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "ril.serialnumber");
            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "ro.serialno");
            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "sys.serialnumber");
            if (serialNumber.equals(""))
                serialNumber = Build.SERIAL;

            // If none of the methods above worked
            if (serialNumber.equals(""))
                serialNumber = null;
        } catch (Exception e) {
            e.printStackTrace();
            serialNumber = null;
        }

        return serialNumber;
    }
}
