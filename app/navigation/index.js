import React from 'react';
import { createStackNavigator, addNavigationHelpers, createAppContainer } from 'react-navigation';
import InformationScreen from '../screens/InformationScreen';
import CameraScreen from '../screens/CameraScreen';

console.disableYellowBox = true;

const AppNavigator = createStackNavigator({
  Camera: { screen: CameraScreen },
  Information: { screen: InformationScreen },
},
  {
    headerMode: 'none',
    mode: 'modal',
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
