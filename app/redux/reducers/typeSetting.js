import { TYPE_SETTING } from '../types';

const initialState = {
  infoSetting: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case TYPE_SETTING:
      console.log('infoSetting', action.infoSetting);
      return {
        ...state,
        infoSetting: action.infoSetting
      }
    default:
      return state;
  }
}