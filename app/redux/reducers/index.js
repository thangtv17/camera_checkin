import { combineReducers } from 'redux';
import typeSetting from './typeSetting';

export default combineReducers({
  typeSetting
});