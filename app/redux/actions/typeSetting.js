import { TYPE_SETTING } from '../types';

export function saveTypeSetting(infoSetting) {
  return dispatch => dispatch({
    type: TYPE_SETTING,
    infoSetting: infoSetting,
  });
}