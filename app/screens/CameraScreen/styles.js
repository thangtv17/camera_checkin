import { Dimensions } from "react-native";
import { ScaledSheet, scale, } from 'react-native-size-matters';
import colors from "../../constants/colors";
import { WIDTH_CROP } from "../../constants/distance";
import { getStatusBarHeight } from "../../components/MyStatusBar";
let { width, height } = Dimensions.get('screen');

export default styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 20,
    alignSelf: 'center',
    position: 'absolute',
    bottom: 20
  },
  containerHeader: {
    width: '100%',
    height: 80,
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 10,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0
  },
  containerHeaderLeft: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  containerHeaderRight: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  styleImg: {
    width: 100,
    height: 50
  },
  styleImgSetting: {
    width: 30,
    height: 30
  },
  textCompanyName: {
    color: colors.colorBlack2,
    fontWeight: 'bold',
    fontSize: 22,
    marginLeft: 10
  },
  textCompanyInfo: {
    color: colors.colorBlack2,
    fontSize: 16,
    marginRight: 10
  },
  containerListButton: {
    position: 'absolute',
    top: (height - 450) / 2 + 40,
    right: 30,
    height: 450,
    justifyContent: 'space-around',
  },
  styleImgCheckin: {
    width: 100,
    height: 100
  },
  containerCrop: {
    width: WIDTH_CROP,
    height: WIDTH_CROP,
    backgroundColor: 'transparent',
    position: 'absolute',
    top: (height - WIDTH_CROP) / 2,
    left: (width - WIDTH_CROP) / 2,
    borderWidth: 3,
    borderRadius: 1,
    borderColor: 'white'
  },
  containerMaskLeft: {
    position: 'absolute',
    width: (width - WIDTH_CROP) / 2,
    height,
    top: 0,
    left: 0,
    bottom: 0,
    backgroundColor: 'white',
    opacity: 1
  },
  containerMaskRight: {
    position: 'absolute',
    width: (width - WIDTH_CROP) / 2,
    height,
    top: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'white',
    opacity: 1
  },
  containerMaskTop: {
    position: 'absolute',
    width: WIDTH_CROP,
    height: (height - WIDTH_CROP) / 2,
    top: 0,
    left: (width - WIDTH_CROP) / 2,
    right: (width - WIDTH_CROP) / 2,
    backgroundColor: 'white',
    opacity: 1
  },
  containerMaskBottom: {
    position: 'absolute',
    width: WIDTH_CROP,
    height: (height - WIDTH_CROP) / 2,
    bottom: 0,
    left: (width - WIDTH_CROP) / 2,
    right: (width - WIDTH_CROP) / 2,
    backgroundColor: 'white',
    opacity: 1
  },
  lineMaskTopLeft: {
    position: 'absolute',
    width: 3,
    height: WIDTH_CROP / 4,
    top: (height - WIDTH_CROP) / 2 - 10,
    left: (width - WIDTH_CROP) / 2 - 10,
    backgroundColor: 'gray',
    borderTopLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  lineMaskTopHeaderLeft: {
    position: 'absolute',
    width: WIDTH_CROP / 4,
    height: 3,
    top: (height - WIDTH_CROP) / 2 - 10,
    left: (width - WIDTH_CROP) / 2 - 10,
    backgroundColor: 'gray',
    borderTopLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  lineMaskTopRight: {
    position: 'absolute',
    width: 3,
    height: WIDTH_CROP / 4,
    top: (height - WIDTH_CROP) / 2 - 10,
    right: (width - WIDTH_CROP) / 2 - 10,
    backgroundColor: 'gray',
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10
  },
  lineMaskTopHeaderRight: {
    position: 'absolute',
    width: WIDTH_CROP / 4,
    height: 3,
    top: (height - WIDTH_CROP) / 2 - 10,
    right: (width - WIDTH_CROP) / 2 - 10,
    backgroundColor: 'gray',
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10
  },
  lineMaskBottomRight: {
    position: 'absolute',
    width: 3,
    height: WIDTH_CROP / 4,
    bottom: (height - WIDTH_CROP) / 2 - 10,
    right: (width - WIDTH_CROP) / 2 - 10,
    backgroundColor: 'gray',
    borderBottomRightRadius: 10,
    borderTopLeftRadius: 10
  },
  lineMaskBottomHeaderRight: {
    position: 'absolute',
    width: WIDTH_CROP / 4,
    height: 3,
    bottom: (height - WIDTH_CROP) / 2 - 10,
    right: (width - WIDTH_CROP) / 2 - 10,
    backgroundColor: 'gray',
    borderTopLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  lineMaskBottomLeft: {
    position: 'absolute',
    width: 3,
    height: WIDTH_CROP / 4,
    bottom: (height - WIDTH_CROP) / 2 - 10,
    left: (width - WIDTH_CROP) / 2 - 10,
    backgroundColor: 'gray',
    borderBottomLeftRadius: 10,
    borderTopRightRadius: 10
  },
  lineMaskBottomHeaderLeft: {
    position: 'absolute',
    width: WIDTH_CROP / 4,
    height: 3,
    bottom: (height - WIDTH_CROP) / 2 - 10,
    left: (width - WIDTH_CROP) / 2 - 10,
    backgroundColor: 'gray',
    borderBottomLeftRadius: 10,
    borderTopRightRadius: 10
  },
});