/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { PermissionsAndroid, Dimensions, Text, View, TouchableOpacity, ImageEditor, Image, StatusBar } from 'react-native';
import { RNCamera } from 'react-native-camera';
const RNFS = require("react-native-fs");
import { decode, encode } from 'base-64';
import TakePictureArray from '../TakePicture';
import { connect } from 'react-redux';
import checkIfFirstLaunch from '../../utils';
import styles from './styles';
import { IC_LOGO, IC_SETTING, IC_CALL, IC_CHECKIN, IC_CHECKOUT } from '../../constants/linkImages';
import { WIDTH_CROP } from '../../constants/distance';
import HTML from 'react-native-render-html';
let { width, height } = Dimensions.get('window');

class CameraScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      uriImage: null,
      infoSetting: null,
      txtResult: ''
    };
  }

  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Cool Photo App Camera Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the camera');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  requestWritePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Cool Photo App Camera Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can write storage');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  // Make sure you import ImageEditor from react-native!
  cropImage = async (uri) => {
    // Construct a crop data object. 
    cropData = {
      offset: { x: (height - WIDTH_CROP) / 2, y: (width - WIDTH_CROP) / 2 },
      size: { width: WIDTH_CROP, height: WIDTH_CROP },
      //  displaySize:{width:20, height:20}, THESE 2 ARE OPTIONAL. 
      //  resizeMode:'contain', 
    }
    // Crop the image. 
    try {
      await ImageEditor.cropImage(uri,
        cropData, (successURI) => {
          console.log('successURI,', successURI)
          // TakePictureArray.getResultCheckin(successURI, result => {
          //   console.log('result', result);
          // })
          this.props.navigation.navigate('Information', {
            uri: successURI
          });
        },
        (error) => { console.log('cropImage,', error) }
      )
    }
    catch (error) {
      console.log('Error caught in this.cropImage:', error)
    }
  }

  takePicture = async (funct) => {
    try {
      // adding activity indicator because of low performance on Android with previous version
      await this.setState({ isCapturing: true })

      const cameraData = await this.camera.takePictureAsync()

      this.setState({
        uriImage: cameraData.uri,
        isCapturing: false
      }, () => {
        // this.cropImage(cameraData.uri);
        if (funct === "checkin") {
          clearTimeout(this.timer)
          TakePictureArray.getResultCheckin(cameraData.uri, result => {
            this.setState({
              txtResult: result.replace('<IOSec>', "").replace('</IOSec>', "").replace('/r/n', "")
            }, () => {
              this.timer = setTimeout(() => {
                this.setState({
                  txtResult: ''
                })
              }, 5000)
            })
          })
        } else if (funct === "checkout") {
          clearTimeout(this.timer)
          TakePictureArray.getResultCheckout(cameraData.uri, result => {
            this.setState({
              txtResult: result.replace('<IOSec>', "").replace('</IOSec>', "")
            }, () => {
              this.timer = setTimeout(() => {
                this.setState({
                  txtResult: ''
                })
              }, 5000)
            })
          })
        }

      })
    } catch (e) {
      // This logs the error
      console.log(e)
    }
  };

  async componentDidMount() {
    StatusBar.setHidden(true);
    const isFirstLaunch = await checkIfFirstLaunch();
    if (isFirstLaunch) {
      this.props.navigation.navigate('Information');
    }
    if (this.props.infoSetting !== null) {
      this.setState({
        infoSetting: JSON.parse(this.props.infoSetting)
      })
    }
    this.requestCameraPermission();
    // this.requestWritePermission();

  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.infoSetting !== prevState.infoSetting) {
      return { infoSetting: JSON.parse(nextProps.infoSetting) }
    }

    return null
  }

  _onClickSetting = () => {
    this.props.navigation.navigate('Information');
  }

  _onClickCall = () => {
    clearTimeout(this.timer)
    TakePictureArray.getResultLogin(result => {
      console.log('result', result.replace('<IOSec>', "").replace('</IOSec>', ""));
      this.setState({
        txtResult: result.replace('<IOSec>', "").replace('</IOSec>', "")
      }, () => {
        this.timer = setTimeout(() => {
          this.setState({
            txtResult: ''
          })
        }, 5000)
      })
    })
  }

  render() {
    let { infoSetting } = this.state;
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.front}
          flashMode={RNCamera.Constants.FlashMode.off}
          autoFocus={true}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          captureAudio={false}
        />
        <View style={styles.containerMaskLeft} />
        <View style={styles.containerMaskRight} />
        <View style={styles.containerMaskTop} />
        <View style={styles.containerMaskBottom} />
        <View style={styles.containerCrop} />
        <View style={styles.lineMaskTopLeft} />
        <View style={styles.lineMaskTopHeaderLeft} />
        <View style={styles.lineMaskTopRight} />
        <View style={styles.lineMaskTopHeaderRight} />
        <View style={styles.lineMaskBottomRight} />
        <View style={styles.lineMaskBottomHeaderRight} />
        <View style={styles.lineMaskBottomLeft} />
        <View style={styles.lineMaskBottomHeaderLeft} />

        <View style={styles.containerHeader}>
          <View style={styles.containerHeaderLeft}>
            <Image
              source={IC_LOGO}
              style={styles.styleImg}
            />
            <Text style={styles.textCompanyName}> {infoSetting !== null ? infoSetting.txtCompanyName : ''} </Text>
          </View>
          <View style={styles.containerHeaderRight}>
            <Text style={styles.textCompanyInfo}> {infoSetting !== null ? infoSetting.txtContactInfo : ''} </Text>
            <TouchableOpacity onPress={this._onClickSetting}>
              <Image
                source={IC_SETTING}
                style={styles.styleImgSetting}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.containerListButton}>
          <TouchableOpacity onPress={this._onClickCall}>
            <Image
              source={IC_CALL}
              style={styles.styleImgCheckin}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.takePicture("checkin")}>
            <Image
              source={IC_CHECKIN}
              style={styles.styleImgCheckin}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.takePicture("checkout")}>
            <Image
              source={IC_CHECKOUT}
              style={styles.styleImgCheckin}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
        </View>
        {this.state.txtResult !== '' ? <View style={styles.capture}>
          <HTML html={this.state.txtResult} />
        </View> : null}
      </View>
    );
  }
}

function mapStateToProps(state) {
  const { infoSetting } = state.typeSetting;
  return {
    infoSetting
  };
}
export default connect(mapStateToProps, null)(CameraScreen);
