import { Dimensions } from "react-native";
import { ScaledSheet, scale, } from 'react-native-size-matters';
import colors from "../../constants/colors";
let { width, height } = Dimensions.get('window');

export default styles = ScaledSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  containerLogo: {
    flexDirection: 'row',
    padding: 5,
    marginHorizontal: 10
  },
  containerTextTitle: {
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    paddingHorizontal: 5,
    width: width / 7,
    marginTop: 3
  },
  textTitle: {
    color: colors.colorBlack2,
  },
  containerTextInput: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.colorBlack2,
    width: '100%',
    height: 40,
    marginLeft: 20,
  },
  edittext: {
    flex: 1,
    textAlignVertical: 'top'
  },
  containerLine: {
    width: width * 0.85,
    height: 1,
    backgroundColor: colors.colorBlack2,
    alignSelf: 'flex-end',
    marginVertical: 10
  },
  containerServerport: {
    flexDirection: 'row',
    flex: 1
  },
  containerReconnect: {
    flexDirection: 'row',
    flex: 1
  },
  textSeconds: {
    color: colors.colorBlack2,
    marginTop: 3,
    marginLeft: 3
  },
  containerRadio: {
    flexDirection: 'row'
  },
  containerButton: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 20
  },
  containerButtonSave: {
    width: 100,
    paddingVertical: 10,
    backgroundColor: colors.colorBlue,
    borderWidth: 1,
    borderColor: colors.colorBlack2,
    borderRadius: 3,
    overflow: 'hidden',
    alignItems: 'center'
  },
  textSave: {
    color: 'white',
    fontWeight: 'bold'
  },
  containerButtonCancel: {
    width: 100,
    paddingVertical: 10,
    backgroundColor: 'gray',
    borderWidth: 1,
    borderColor: colors.colorBlack2,
    borderRadius: 3,
    overflow: 'hidden',
    alignItems: 'center',
    marginLeft: 30
  },
});