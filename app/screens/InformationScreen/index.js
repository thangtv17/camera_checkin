import React from 'react';
import {
  Image,
  Platform,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  TextInput
} from 'react-native';
import styles from './styles';
import colors from "../../constants/colors";
import { connect } from 'react-redux';
import { RadioButton } from 'react-native-btr';
import { saveTypeSetting } from '../../redux/actions/typeSetting';
import { saveData, getData } from '../../constants/storage';


class InformationScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      txtLogo: '',
      txtCompanyName: '',
      txtContactInfo: '',
      txtServerAddr: '',
      txtServerPort: '',
      txtReConnect: '',
      isCallPerson: true,
      txtCameraErr: '',
      txtServerErr: '',
      txtExcreption: ''
    };
    this.onChangeRadioButton = this.onChangeRadioButton.bind(this);
  }

  componentDidMount() {
    StatusBar.setHidden(true);
    if (this.props.infoSetting !== null) {
      let { txtLogo, txtCompanyName, txtContactInfo, txtServerAddr, txtServerPort, txtReConnect,
        isCallPerson, txtCameraErr, txtServerErr, txtExcreption } = JSON.parse(this.props.infoSetting);
      this.setState({
        txtLogo,
        txtCompanyName,
        txtContactInfo,
        txtServerAddr,
        txtServerPort,
        txtReConnect,
        isCallPerson,
        txtCameraErr,
        txtServerErr,
        txtExcreption
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.txtLogo !== this.state.txtLogo)
      return true;
    if (nextState.txtCompanyName !== this.state.txtCompanyName)
      return true;
    if (nextState.txtContactInfo !== this.state.txtContactInfo)
      return true;
    if (nextState.txtServerAddr !== this.state.txtServerAddr)
      return true;
    if (nextState.txtServerPort !== this.state.txtServerPort)
      return true;
    if (nextState.txtReConnect !== this.state.txtReConnect)
      return true;
    if (nextState.isCallPerson !== this.state.isCallPerson)
      return true;
    if (nextState.txtCameraErr !== this.state.txtCameraErr)
      return true;
    if (nextState.txtServerErr !== this.state.txtServerErr)
      return true;
    if (nextState.txtExcreption !== this.state.txtExcreption)
      return true;
    return false;
  }

  _goBack = () => {
    this.props.navigation.goBack();
  }

  onChangeTextLogo = (txtLogo) => {
    this.setState({
      txtLogo
    })
  }

  onChangeTextCompanyName = (txtCompanyName) => {
    this.setState({
      txtCompanyName
    })
  }

  onChangeTextContactInfo = (txtContactInfo) => {
    this.setState({
      txtContactInfo
    })
  }

  onChangeTextServerAddr = (txtServerAddr) => {
    this.setState({
      txtServerAddr
    })
  }

  onChangeTextServerPort = (txtServerPort) => {
    this.setState({
      txtServerPort
    })
  }

  onChangeTextReConnect = (txtReConnect) => {
    this.setState({
      txtReConnect
    })
  }

  onChangeRadioButton = (isCallPerson) => () => {
    this.setState({
      isCallPerson
    })
  }

  onChangeTextCameraErr = (txtCameraErr) => {
    this.setState({
      txtCameraErr
    })
  }

  onChangeTextServerErr = (txtServerErr) => {
    this.setState({
      txtServerErr
    })
  }

  onChangeTextException = (txtExcreption) => {
    this.setState({
      txtExcreption
    })
  }

  onSave = (txtLogo, txtCompanyName, txtContactInfo,
    txtServerAddr, txtServerPort, txtReConnect,
    isCallPerson, txtCameraErr, txtServerErr,
    txtExcreption) => () => {
      let infoSetting = {
        txtLogo,
        txtCompanyName,
        txtContactInfo,
        txtServerAddr,
        txtServerPort,
        txtReConnect,
        isCallPerson,
        txtCameraErr,
        txtServerErr,
        txtExcreption
      }
      this.props.saveTypeSetting(JSON.stringify(infoSetting))
      this._goBack();
    }

  onCancel = () => {
    this._goBack();
  }

  render() {
    let { txtLogo, txtCompanyName, txtContactInfo, txtServerAddr, txtServerPort, txtReConnect,
      isCallPerson, txtCameraErr, txtServerErr, txtExcreption } = this.state;
    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View style={styles.containerLogo}>
            <View style={styles.containerTextTitle}>
              <Text style={styles.textTitle}>Logo</Text>
            </View>
            <View style={styles.containerTextInput}>
              <TextInput
                style={styles.edittext}
                onChangeText={(text) => this.onChangeTextLogo(text)}
                value={this.state.txtLogo}
              />
            </View>
          </View>
          <View style={styles.containerLogo}>
            <View style={styles.containerTextTitle}>
              <Text style={styles.textTitle}>Company Name</Text>
            </View>
            <View style={styles.containerTextInput}>
              <TextInput
                style={styles.edittext}
                onChangeText={(text) => this.onChangeTextCompanyName(text)}
                value={this.state.txtCompanyName}
              />
            </View>
          </View>
          <View style={styles.containerLogo}>
            <View style={styles.containerTextTitle}>
              <Text style={styles.textTitle}>Contact infor</Text>
            </View>
            <View style={[styles.containerTextInput, { height: 100 }]}>
              <TextInput
                style={styles.edittext}
                onChangeText={(text) => this.onChangeTextContactInfo(text)}
                value={this.state.txtContactInfo}
                multiline={true}
              />
            </View>
          </View>
          <View style={styles.containerLine} />
          <View style={styles.containerLogo}>
            <View style={styles.containerTextTitle}>
              <Text style={styles.textTitle}>Server addr</Text>
            </View>
            <View style={styles.containerTextInput}>
              <TextInput
                style={styles.edittext}
                onChangeText={(text) => this.onChangeTextServerAddr(text)}
                value={this.state.txtServerAddr}
              />
            </View>
          </View>
          <View style={styles.containerLogo}>
            <View style={styles.containerServerport}>
              <View style={styles.containerTextTitle}>
                <Text style={styles.textTitle}>Server port</Text>
              </View>
              <View style={styles.containerTextInput}>
                <TextInput
                  style={styles.edittext}
                  onChangeText={(text) => this.onChangeTextServerPort(text)}
                  value={this.state.txtServerPort}
                />
              </View>
            </View>
            <View style={styles.containerReconnect}>
              <View style={styles.containerTextTitle}>
                <Text style={styles.textTitle}>Re-connect interval</Text>
              </View>
              <View style={styles.containerTextInput}>
                <TextInput
                  style={styles.edittext}
                  onChangeText={(text) => this.onChangeTextReConnect(text)}
                  value={this.state.txtReConnect}
                />
              </View>
              <View style={styles.textSeconds}>
                <Text style={styles.textTitle}>(seconds)</Text>
              </View>
            </View>
          </View>
          <View style={styles.containerLogo}>
            <View style={[styles.containerTextTitle, { marginRight: 15 }]}>
              <Text style={styles.textTitle}>When call</Text>
            </View>
            <TouchableOpacity style={[styles.containerRadio, { marginRight: 30 }]} onPress={this.onChangeRadioButton(true)}>
              <RadioButton
                checked={isCallPerson}
                color={isCallPerson ? colors.colorRed : colors.colorBlack2}
                onPress={this.onChangeRadioButton(true)}
              />
              <Text style={[styles.textTitle, { marginTop: 3, marginLeft: 20 }]}>Call to contact person</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.containerRadio} onPress={this.onChangeRadioButton(false)}>
              <RadioButton
                checked={!isCallPerson}
                color={!isCallPerson ? colors.colorRed : colors.colorBlack2}
                onPress={this.onChangeRadioButton(false)}
              />
              <Text style={[styles.textTitle, { marginTop: 3, marginLeft: 20 }]}>Call server API</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.containerLine} />
          <View style={styles.containerLogo}>
            <View style={styles.containerTextTitle}>
              <Text style={styles.textTitle}>Camera error msg</Text>
            </View>
            <View style={styles.containerTextInput}>
              <TextInput
                style={styles.edittext}
                onChangeText={(text) => this.onChangeTextCameraErr(text)}
                value={this.state.txtCameraErr}
              />
            </View>
          </View>
          <View style={styles.containerLogo}>
            <View style={styles.containerTextTitle}>
              <Text style={styles.textTitle}>Server error msg</Text>
            </View>
            <View style={styles.containerTextInput}>
              <TextInput
                style={styles.edittext}
                onChangeText={(text) => this.onChangeTextServerErr(text)}
                value={this.state.txtServerErr}
              />
            </View>
          </View>
          <View style={styles.containerLogo}>
            <View style={styles.containerTextTitle}>
              <Text style={styles.textTitle}>Exception error msg</Text>
            </View>
            <View style={styles.containerTextInput}>
              <TextInput
                style={styles.edittext}
                onChangeText={(text) => this.onChangeTextException(text)}
                value={this.state.txtExcreption}
              />
            </View>
          </View>
          <View style={styles.containerButton}>
            <TouchableOpacity
              style={styles.containerButtonSave}
              onPress={this.onSave(txtLogo, txtCompanyName, txtContactInfo, txtServerAddr, txtServerPort, txtReConnect, isCallPerson, txtCameraErr, txtServerErr, txtExcreption)}
            >
              <Text style={styles.textSave}>SAVE</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.containerButtonCancel} onPress={this.onCancel}>
              <Text style={styles.textSave}>CANCEL</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* <Image 
          style={{width: 500, height: 500}}
          source={{uri: this.props.navigation.getParam('uri', null)}}
        /> */}
      </ScrollView>
    );
  }
}

const mapDispatchToProps = {
  saveTypeSetting
};

function mapStateToProps(state) {
  const { infoSetting } = state.typeSetting;
  return {
    infoSetting
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(InformationScreen);

