export const IC_CALL = require('../assets/icons/icon_call.png');
export const IC_CHECKIN = require('../assets/icons/icon_checkin.png');
export const IC_CHECKOUT = require('../assets/icons/icon_checkout.png');
export const IC_LOGO = require('../assets/icons/icon_logo.png');
export const IC_SETTING = require('../assets/icons/icon_setting.png');
