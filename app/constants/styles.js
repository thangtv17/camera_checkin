import { StyleSheet, Dimensions, Platform } from 'react-native';
import { height, width } from './distance';
import height from './distance';

export const globalStyles = StyleSheet.create({
  styleTextToolbar: {
    fontSize: width / 24,
    // fontFamily: Platform.OS === 'android' ? 'segoeui' : 'SegoeUIBlack',
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center'
  },
  styleTextQuestion: {
    fontSize: width / 26,
    // fontFamily: Platform.OS === 'android' ? 'segoeui' : 'SegoeUIBlack',
    fontWeight: 'bold',
    paddingHorizontal: 10,
    color: colors.colorBlack2,
    marginTop: 5
  },
  styleImageQuesion: {
    marginTop: 10
  },
  styleContainerAnswer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 15
  },
  styleDrawerHeaderText: {
    fontSize: 14,
    // fontFamily: Platform.OS === 'android' ? 'segoeuil' : 'SegoeUI-Light',
    color: 'white',
    textAlign: 'center'
  },
  styleTextNumber: {
    fontSize: 14,
    // fontFamily: Platform.OS === 'android' ? 'segoeuil' : 'SegoeUI-Light',
    textAlign: 'center'
  },
  styleTextTabbar: {
    fontSize: 14,
    // fontFamily: Platform.OS === 'android' ? 'segoeui' : 'SegoeUIBlack',
    fontWeight: 'bold',
    color: colors.colorBlack2
  },
  styleViewNumber: {
    width: 30,
    height: 30,
    borderRadius: 15,
    borderWidth: 1,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10
  },
  styleItemDrawerView: {
    width: width * 0.1,
    height: width * 0.1,
    borderRadius: width * 0.05,
    marginHorizontal: width * 0.04,
    marginVertical: width * 0.02,
    justifyContent: 'center',
    alignItems: 'center'
  },
  styleDrawerView: {
    width: width,
    height: height,
    backgroundColor: 'white'
  },
  styleHeaderDrawerView: {
    flexDirection: 'row',
    width: width,
    height: height * 0.1,
    backgroundColor: colors.colorPrimaryDark,
    justifyContent: 'center',
    alignItems: 'center'
  },
  styleContentDrawerView: {
    alignItems: 'center',
    paddingBottom: height * 0.2
  },
  containerHeaderText: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  styleTextExplan: {
    fontSize: 16,
    // fontFamily: Platform.OS === 'android' ? 'seguili' : 'SegoeUI-Light',
    fontStyle: 'italic',
    color: colors.colorBlack3,
    marginTop: 15,
    marginHorizontal: 20
  },
  styleTextAnswer: {
    fontSize: width / 26,
    // fontFamily: Platform.OS === 'android' ? 'segoeuil' : 'SegoeUI-Light',
    color: colors.colorBlack3,
  },
  styleContainerContent: {
    flex: 1,
    backgroundColor: 'white',
    // paddingHorizontal: 10,
    // marginTop: 10
    marginBottom: height * 0.1 + 10
  },
});