import { Dimensions } from 'react-native';

export const { width, height } = Dimensions.get('screen');

export const WIDTH_CROP = width / 2.6;